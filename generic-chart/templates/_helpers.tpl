{{/*
Expand the name of the chart.
*/}}
{{- define "chart.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "chart.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "chart.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "chart.labels" -}}
{{ include "chart.selectorLabels" . }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
helm.sh/chart: {{ include "chart.chart" . }}
version: {{ .Chart.AppVersion | quote }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "chart.selectorLabels" -}}
app.kubernetes.io/name: {{ include "chart.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
app: {{ include "chart.fullname" $ }}
{{- end }}

{{/*
Define Service name
*/}}
{{- define "chart.serviceName" -}}
{{ include "chart.fullname" $ }}
{{- end }}


{{/*
Create the name of the service account to use
*/}}
{{- define "chart.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "chart.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{- define "probeSpec" }}
            initialDelaySeconds: {{ .initialDelaySeconds }}
            periodSeconds: {{ .periodSeconds }}
            {{- if .timeoutSeconds }}
            timeoutSeconds: {{ .timeoutSeconds }}
            {{- end }}
            {{- if .successThreshold }}
            successThreshold: {{ .successThreshold }}
            {{- end }}
            {{- if .failureThreshold }}
            failureThreshold: {{ .failureThreshold }}
            {{- end }}
            {{ .type }}:
              {{- if .port }}
              port: {{ .port }}
              {{- end }}
              {{- if .path }}
              path: {{ .path }}
              {{- end }}
              {{- if .httpHeaders }}
              httpHeaders:
                {{- range $header := .httpHeaders }}
                - name: {{ $header.name | quote}}
                  value: {{ $header.value | quote }}
                {{- end }}
              {{- end }}
              {{- if .command }}
              command:
                {{- range .command }}
                - {{ . }}
                {{- end }}
              {{- end }}
{{- end -}}

{{ define "podSpec" }}
    spec:
      {{- if .Values.podSecurityContext }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 10 }}
      {{- end }}
      {{- if or .Values.existingVolumeClaims .Values.configMap .Values.secret .Values.externalSecret .Values.emptyDir .Values.emptyMemoryDir }}
      volumes:
        {{- range $index, $pvc := .Values.existingVolumeClaims }}
        - name: {{ $pvc.volumeName }}
          persistentVolumeClaim:
            claimName: {{ $pvc.pvcName }}
        {{- end }}
        {{- with .Values.configMap }}
        - name: {{ .volumeName }}
          configMap:
            name: {{ default (include "chart.fullname" $) .name }}
        {{- end }}
        {{- with .Values.secret }}
        - name: {{ .volumeName }}
          secret:
            secretName: {{ default (include "chart.fullname" $) .name }}
        {{- end }}
        {{- with .Values.externalSecret }}
        - name: {{ .volumeName }}
          secret:
            secretName: {{ default (include "chart.fullname" $) .name }}
        {{- end }}
        {{- with .Values.emptyDir }}
        - name: {{ .volumeName }}
          emptyDir: {}
        {{- end }}
        {{- with .Values.emptyMemoryDir }}
        - name: {{ .volumeName }}
          emptyDir:
            medium: Memory
        {{- end }}
      {{- end }}
      serviceAccountName: {{ include "chart.serviceAccountName" $ }}
      {{- with .Values.nodeSelector }}
      nodeSelector: {{ toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity: {{ toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations: {{ toYaml . | nindent 8 }}
      {{- end }}
      {{- if .Values.initContainers }}
      initContainers:
      {{- range $init := .Values.initContainers }}
        - name: {{ $init.name }}
          image: {{ $init.image }}
          imagePullPolicy: {{ $init.imagePullPolicy }}
          {{- if $init.command }}
          command:
            {{- range $init.command }}
            -  {{ . }}
            {{- end }}
          {{- end }}
          {{- if $init.args }}
          args:
            {{- range $init.args }}
            -  {{ . }}
            {{- end }}
          {{- end }}
          {{- with $init.volumeMounts }}
          volumeMounts: {{ toYaml . | nindent 12 }}
          {{- end -}}
          {{- if or $init.env $init.secretEnv }}
          env:
            {{- range $env := $init.env }}
            - name: {{ $env.name | quote }}
              value: {{ $env.value | quote }}
          {{- end }}
          {{- with $init.secretEnv }}
            {{- range $secretDefinition := . }}
            {{- if not (empty $secretDefinition) }}
            - name: {{ $secretDefinition.name | quote }}
              valueFrom:
                secretKeyRef:
                  name: {{ $secretDefinition.secret | quote }}
                  key: {{ $secretDefinition.key | quote }}
            {{- end }}
            {{- end }}
          {{- end }}
          {{- end }}
      {{- end }}
      {{- end }}
      containers:
        {{- with .Values.mainContainer }}
        - name: {{ .name }}
          image: {{ .image }}
          imagePullPolicy: {{ .imagePullPolicy }}
          {{- if .command }}
          command:
            {{- range .command }}
            -  {{ . }}
            {{- end }}
          {{- end }}
          {{- if .args }}
          args:
            {{- range .args }}
            -  {{ . }}
            {{- end }}
          {{- end }}
          {{- with .volumeMounts }}
          volumeMounts: {{ toYaml . | nindent 12 }}
          {{- end -}}
          {{- if or .env .secretEnv }}
          env:
            {{- range $env := .env }}
            - name: {{ $env.name | quote }}
              value: {{ $env.value | quote }}
          {{- end }}
          {{- with .secretEnv }}
            {{- range $secretDefinition := . }}
            {{- if not (empty $secretDefinition) }}
            - name: {{ $secretDefinition.name | quote }}
              valueFrom:
                secretKeyRef:
                  name: {{ $secretDefinition.secret | quote }}
                  key: {{ $secretDefinition.key | quote }}
            {{- end }}
            {{- end }}
          {{- end }}
          {{- end }}
          {{- with .resources }}
          resources: {{ toYaml . | nindent 12 }}
          {{- end }}
          {{- if .securityContext.enabled }}
          securityContext:
            {{- toYaml .securityContext | nindent 14 }}
          {{- end }}
          ports:
            {{- range $port := .containerPorts }}
              - name: {{ $port.name }}
                containerPort: {{ $port.containerPort }}
                protocol: {{ $port.protocol }}
            {{- end }}
          {{- if .livenessProbe.enabled }}
          {{- with .livenessProbe }}
          livenessProbe:
            {{- template "probeSpec" . }}
          {{- end }}
          {{- end }}
          {{- if .readinessProbe.enabled }}
          {{- with .readinessProbe }}
          readinessProbe:
            {{- template "probeSpec" . }}
          {{- end }}
          {{- end }}
      {{- end }}
      {{- if .Values.imagePullSecrets }}
      imagePullSecrets:
      {{- range .Values.imagePullSecrets }}
        - name: {{ . }}
      {{- end }}
      {{- end }}
{{- end -}}